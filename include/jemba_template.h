/**
 * Brief description of the jemba_template.
 *
 * @file jemba_template.h
 *
 */
#ifndef _JEMBA_TEMPLATE_H_
#define _JEMBA_TEMPLATE_H_

#if defined(__cpu08__)
typedef          char       char_t;
typedef signed   char       int8_t;
typedef signed   int        int16_t;
typedef signed   long int   int32_t;
typedef unsigned char       uint8_t;
typedef unsigned int        uint16_t;
typedef unsigned long int   uint32_t;
#else
#include <stdint.h>
#endif /* __cpu08__ */


/* ************************************************************************** */
/* DEFINITONS                                                                 */
/* ************************************************************************** */


/* ************************************************************************** */
/* TYPE DEFINITIONS                                                           */
/* ************************************************************************** */


/* ************************************************************************** */
/* CONTEXTS                                                                   */
/* ************************************************************************** */

typedef struct board_ctx_t {
  uint8_t initialized;      /**< Flag to know if the instance is correctly initialized. */
  int32_t dummy;            /**< Dummy context value. */
} JEMBA_PREFIX_CTX_T;

/* ************************************************************************** */
/* PUBLIC VARIABLES                                                           */
/* ************************************************************************** */

extern JEMBA_PREFIX_CTX_T JEMBA_PREFIX_ctx;

/* ************************************************************************** */
/* PUBLIC FUNCTIONS                                                           */
/* ************************************************************************** */

#ifdef __cplusplus
extern "C" {
#endif /* __cpluplus */

extern void JEMBA_PREFIX_init (JEMBA_PREFIX_CTX_T* pCtx, int32_t dummy);

extern void JEMBA_PREFIX_deinit (JEMBA_PREFIX_CTX_T* pCtx);

extern void JEMBA_PREFIX_poll (JEMBA_PREFIX_CTX_T* pCtx);

#ifdef __cplusplus
}
#endif /* __cpluplus */

#endif /* _JEMBA_TEMPLATE_H_ */
