/**
 * Runner of test suites.
 *
 * @file test_runner.c
 *
 */
#include <stddef.h>
#include <string.h>
#include "test_runner.h"

/* Module tests */
#include "jemba_template_test.h"

/* ************************************************************************** */
/* DEFINITIONS                                                                */
/* ************************************************************************** */


/* ************************************************************************** */
/* TYPE DEFINITIONS                                                           */
/* ************************************************************************** */

/* ************************************************************************** */
/* CONSTANTS                                                                  */
/* ************************************************************************** */

/**
 * List of module tests.
 */
static const TEST_RUNNER_TEST_MODULE_PF module_tests[] = {
    JEMBA_PREFIX_TEST_test,

    /* Sentinel */
    NULL
};

/* ************************************************************************** */
/* PUBLIC VARIABLES                                                           */
/* ************************************************************************** */


/* ************************************************************************** */
/* LOCAL VARIABLES                                                            */
/* ************************************************************************** */


/* ************************************************************************** */
/* STATIC FUNCTION DECLARATIONS                                               */
/* ************************************************************************** */

static void setup (void);
static void teardown (void);

/* ************************************************************************** */
/* PUBLIC FUNCTIONS                                                           */
/* ************************************************************************** */

/**
 * Module test runner.
 *
 * Runs all module tests included in #module_tests.
 * It also performs a test initialization (#setup) and de-initialization
 * (#teardown) of the entire bunch of module tests.
 *
 * @return Overall result of the tests.
 * @retval 0  Some test module failed.
 * @retval 1  All test modules passed.
 *
 */
uint32_t TEST_RUNNER_run (void)
{
  uint32_t result = 0;
  TEST_RUNNER_TEST_MODULE_PF module_test =
      (TEST_RUNNER_TEST_MODULE_PF) module_tests[0];

  setup();

  while (module_test != NULL) {
    result &= module_test();
    module_test++;
  }

  teardown();

  return result;
}

/* ************************************************************************** */
/* STATIC FUNCTIONS                                                           */
/* ************************************************************************** */

/**
 * Test suite initialization.
 *
 */
static void setup (void)
{

}


/**
 * Test suite de-initialization.
 */
static void teardown (void)
{

}
