/**
 * Entry point of the test application.
 *
 * @file main.c
 *
 */
#include <stdlib.h>
#include "test_runner.h"


/**
 * Main function.
 *
 * Executes the test runner which returns the result of the overall test bunch.
 * The application will exit successfully if the result is 1, otherwise it will
 * exit with a failure.
 *
 * @return Overall result of the tests.
 * @retval 0  Some test failed.
 * @retval 1  All tests passed.
 *
 */
int main (void)
{
  uint32_t result;

  result = TEST_RUNNER_run();

  return (result == 1) ? EXIT_SUCCESS : EXIT_FAILURE;
}
