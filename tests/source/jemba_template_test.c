/**
 * Suite of tests to exercise the correct behavior of jemba_template.
 *
 * @file jemba_template_test.c
 *
 */
#include <stddef.h>
#include <string.h>
#include "jemba_template_test.h"
#include "jemba_template.h"


/* ************************************************************************** */
/* DEFINITIONS                                                                */
/* ************************************************************************** */


/* ************************************************************************** */
/* TYPE DEFINITIONS                                                           */
/* ************************************************************************** */


/* ************************************************************************** */
/* CONSTANTS                                                                  */
/* ************************************************************************** */


/* ************************************************************************** */
/* PUBLIC VARIABLES                                                           */
/* ************************************************************************** */


/* ************************************************************************** */
/* LOCAL VARIABLES                                                            */
/* ************************************************************************** */


/* ************************************************************************** */
/* STATIC FUNCTION DECLARATIONS                                               */
/* ************************************************************************** */


/* ************************************************************************** */
/* PUBLIC FUNCTIONS                                                           */
/* ************************************************************************** */

/**
 * Exercise the behavior of module jemba_template.
 *
 *
 * @implements
 * This function implements #TEST_RUNNER_TEST_MODULE_PF.
 *
 * @return Result of the tests.
 * @retval 0  Some test cases failed.
 * @retval 1  All test cases passed.
 *
 */
uint32_t JEMBA_PREFIX_TEST_test (void)
{
  uint32_t result;
  JEMBA_PREFIX_CTX_T ctx;

  JEMBA_PREFIX_init(&ctx, 0x5A5A5A5A);

  result = ctx.dummy == 0x5A5A5A5A;
  result &= ctx.initialized == 1;

  JEMBA_PREFIX_deinit(&ctx);

  result &= ctx.initialized == 0;

  return result;
}
