/**
 * Runner of test suites.
 *
 * @file test_runner.h
 *
 */
#ifndef _TEST_RUNNER_H_
#define _TEST_RUNNER_H_

#if defined(__cpu08__)
typedef          char       char_t;
typedef signed   char       int8_t;
typedef signed   int        int16_t;
typedef signed   long int   int32_t;
typedef unsigned char       uint8_t;
typedef unsigned int        uint16_t;
typedef unsigned long int   uint32_t;
#else
#include <stdint.h>
#endif /* __cpu08__ */



/* ************************************************************************** */
/* DEFINITONS                                                                 */
/* ************************************************************************** */


/* ************************************************************************** */
/* TYPE DEFINITIONS                                                           */
/* ************************************************************************** */

/**
 * Function interface to be implemented by all test modules.
 *
 * @return Result of the tests.
 * @retval 0  Some test cases failed.
 * @retval 1  All test cases passed.
 *
 */
typedef uint32_t (*TEST_RUNNER_TEST_MODULE_PF) (void);

/* ************************************************************************** */
/* CONTEXTS                                                                   */
/* ************************************************************************** */


/* ************************************************************************** */
/* PUBLIC FUNCTIONS                                                           */
/* ************************************************************************** */

#ifdef __cplusplus
extern "C" {
#endif /* __cpluplus */

extern uint32_t TEST_RUNNER_run (void);

#ifdef __cplusplus
}
#endif /* __cpluplus */

#endif /* _TEST_RUNNER_H_ */
