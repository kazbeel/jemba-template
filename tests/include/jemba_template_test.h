/**
 * Suite of tests to exercise the correct behavior of jemba_template.
 *
 * @file jemba_template_test.h
 *
 */
#ifndef _JEMBA_TEMPLATE_TEST_H_
#define _JEMBA_TEMPLATE_TEST_H_

#if defined(__cpu08__)
typedef          char       char_t;
typedef signed   char       int8_t;
typedef signed   int        int16_t;
typedef signed   long int   int32_t;
typedef unsigned char       uint8_t;
typedef unsigned int        uint16_t;
typedef unsigned long int   uint32_t;
#else
#include <stdint.h>
#endif /* __cpu08__ */



/* ************************************************************************** */
/* DEFINITONS                                                                 */
/* ************************************************************************** */


/* ************************************************************************** */
/* TYPE DEFINITIONS                                                           */
/* ************************************************************************** */


/* ************************************************************************** */
/* CONTEXTS                                                                   */
/* ************************************************************************** */


/* ************************************************************************** */
/* PUBLIC FUNCTIONS                                                           */
/* ************************************************************************** */

#ifdef __cplusplus
extern "C" {
#endif /* __cpluplus */

extern uint32_t JEMBA_PREFIX_TEST_test (void);

#ifdef __cplusplus
}
#endif /* __cpluplus */

#endif /* _JEMBA_TEMPLATE_TEST_H_ */
