Introduction {#mainpage}
============

This is a demo application created to demonstrate how the tool `jemba` works.

@image html gnu_hornedword.png "GNU run free run GNU" width=100

# Features {#features}

Did someone say features?

    - Feature 1
    - ...

# Release Notes {#release_notes}

## Version (release date)

New Features:

    - Feature A.
    - ...
    
Bug Fixes:

    - Fixed A.

Other Changes:

    - Change A.

# License {#license}

The license you choose for your project, if any.