/**
 * Brief description of the jemba_template.
 *
 * @file jemba_template.c
 *
 */
#include <assert.h>
#include <stddef.h>
#include <string.h>
#include "jemba_template.h"

/* ************************************************************************** */
/* DEFINITIONS                                                                */
/* ************************************************************************** */


/* ************************************************************************** */
/* TYPE DEFINITIONS                                                           */
/* ************************************************************************** */


/* ************************************************************************** */
/* CONSTANTS                                                                  */
/* ************************************************************************** */


/* ************************************************************************** */
/* PUBLIC VARIABLES                                                           */
/* ************************************************************************** */


/* ************************************************************************** */
/* LOCAL VARIABLES                                                            */
/* ************************************************************************** */

/* ************************************************************************** */
/* STATIC FUNCTION DECLARATIONS                                               */
/* ************************************************************************** */


/* ************************************************************************** */
/* PUBLIC FUNCTIONS                                                           */
/* ************************************************************************** */

/**
 * Initialization.
 *
 * @param[in,out] pCtx    Pointer to the context.
 * @param[in]     dummy   Dummy value.
 *
 */
void JEMBA_PREFIX_init (JEMBA_PREFIX_CTX_T* pCtx, int32_t dummy)
{
  assert(pCtx != NULL);
  assert(dummy > 0);

  memset(pCtx, 0, sizeof(JEMBA_PREFIX_CTX_T));

  pCtx->dummy = dummy;
  pCtx->initialized = 1;
}

/**
 * De-Initialization.
 *
 * @param[in,out] pCtx    Pointer to the context.
 *
 */
void JEMBA_PREFIX_deinit (JEMBA_PREFIX_CTX_T* pCtx)
{
  assert(pCtx != NULL);

  pCtx->initialized = 0;
}

/**
 * Brain of the library.
 *
 * @note
 * This function must be called in poll mode or be a task in a RTOS.
 *
 * @param[in,out] pCtx    Pointer to the context.
 *
 */
void JEMBA_PREFIX_poll (JEMBA_PREFIX_CTX_T* pCtx)
{
  assert(pCtx != NULL);
}
